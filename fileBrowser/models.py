'''
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class File(models.Model):
    filename = models.CharField(max_length=200, help_text='filename')
    owner = models.CharField(max_length=20)
    file_data = models.FileField(blank=True, default='')
    date_created = models.DateTimeField()
    last_modified = models.DateTimeField()
    last_accessed = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="file", null=True)

    class Meta:
        ordering = ['last_accessed']

    def __str__(self):
        return self.filename
'''

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.urls import reverse

class File(models.Model):
    filename = models.CharField(max_length=200, help_text='filename')
    file_data = models.FileField(blank=True, default='')
    date_created  = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['date_created']

    def __str__(self):
        return self.filename
