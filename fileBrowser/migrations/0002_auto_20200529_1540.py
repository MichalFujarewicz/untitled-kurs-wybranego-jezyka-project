# Generated by Django 3.0.6 on 2020-05-29 13:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fileBrowser', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='file',
            old_name='file',
            new_name='file_data',
        ),
    ]
