# Generated by Django 3.0.6 on 2020-05-29 13:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fileBrowser', '0002_auto_20200529_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='file_data',
            field=models.BinaryField(editable=True),
        ),
    ]
