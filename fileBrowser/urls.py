'''
from django.urls import path, include
from . import views
from rest_framework import routers
from django.conf.urls import url

router = routers.DefaultRouter()
router.register('files', views.FilesView)

urlpatterns = [
    path('', include(router.urls)),
    path("view/", views.view, name="view"),
    url('files', views.home, name='home'),
    url('uploads/form/', views.model_form_upload, name='model_form_upload'),
]
'''

from django.urls import path, include
from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'files', views.home, name='home'),
    url(r'uploads/form/', views.model_form_upload, name='model_form_upload'),
]
