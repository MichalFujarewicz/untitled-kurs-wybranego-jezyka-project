from rest_framework import serializers
from .models import File

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('id', 'filename', 'owner', 'file_data', 'date_created', 'last_modified', 'last_accessed')

