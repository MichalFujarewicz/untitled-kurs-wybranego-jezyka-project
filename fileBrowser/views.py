from django.http import HttpResponse
from django.shortcuts import render, redirect
from rest_framework import viewsets
from .models import File
from .serializers import FileSerializer
from .forms import DocumentForm
'''
def view(response):
    return render(response, "main/view.html", {})

class FilesView(viewsets.ModelViewSet):
    queryset = File.objects.all()
    serializer_class = FileSerializer
'''




def home(request):
    documents = File.objects.all()
    return render(request, 'home.html', { 'documents': documents })

def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = DocumentForm()
    return render(request, 'model_form_upload.html', {
        'form': form
    })
